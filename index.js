db.fruits.aggregate([
    { $match: {onSale: true}},
    { $group: { _id: null, total: { $sum: "$stock"}} }
]);

db.fruits.aggregate([
    { $match: {onSale: true, "stock": { $gt: 20 } } },
    { $group: { _id: null, total: { $sum: "$stock"}} }
]);

db.fruits.aggregate([
    { $match: {onSale: true}},
    { $group: {_id: null, average_price:{$avg: "$price"}}  }
]);


db.fruits.aggregate([
    { $match: {onSale: true}},
    { $group: {_id: null, max_price:{$max: "$price"}}  }
]);
// OR (without checking if they are on sale)
db.fruits.aggregate([
    { $group: {_id: null, max_price:{$max: "$price"}}  }
]);